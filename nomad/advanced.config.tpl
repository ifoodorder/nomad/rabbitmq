[
  {ssl, [
	  {cacertfile, "/local/ca_chain.pem"},
	  {certfile, "/local/cert.pem"},
	  {keyfile, "/secrets/key.pem"},
	  {verify, verify_peer},
	  {server_name_indication, disable}
  ]},
  {rabbitmq_auth_backend_ldap,
    [  {group_lookup_base, "cn=groups,cn=accounts,dc=ifoodorder,dc=com"},
       {tag_queries,
         [
           {management, {constant, true}},
           {administrator, {in_group, "cn=rabbitmq-admin,cn=groups,cn=accounts,dc=ifoodorder,dc=com"}}
         ]
       }
    ]
  }
].
