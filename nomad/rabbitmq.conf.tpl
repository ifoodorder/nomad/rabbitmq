listeners.tcp = none
listeners.ssl.default = 0.0.0.0:5671
auth_backends.1 = ldap
auth_backends.2 = internal
auth_ldap.servers.1 = ipa.ifoodorder.com
auth_ldap.timeout = 15000
auth_ldap.use_ssl = true
auth_ldap.port = 636
auth_ldap.dn_lookup_bind.user_dn = uid=rabbitmq,cn=sysaccounts,cn=etc,dc=ifoodorder,dc=com
auth_ldap.dn_lookup_bind.password = {{ with secret "secret/rabbitmq/ldap" }}{{ .Data.password }}{{ end }}
auth_ldap.dn_lookup_attribute = uid
auth_ldap.dn_lookup_base = cn=users,cn=accounts,dc=ifoodorder,dc=com

management.ssl.port = 15672
management.ssl.cacertfile = /local/ca_chain.pem
management.ssl.certfile = /local/cert.pem
management.ssl.keyfile = /secrets/key.pem
ssl_options.cacertfile = /local/ca_chain.pem
ssl_options.certfile = /local/cert.pem
ssl_options.keyfile = /secrets/key.pem
ssl_options.verify = verify_peer
ssl_options.depth  = 3
ssl_options.fail_if_no_peer_cert = true
auth_ldap.log = network

cluster_formation.peer_discovery_backend = consul
cluster_formation.consul.acl_token = {{ with secret "consul/creds/rabbitmq" }}{{ .Data.token }}{{ end }}
cluster_formation.consul.host = {{ env "attr.unique.network.ip-address" }}
cluster_formation.consul.port = 8501
cluster_formation.consul.scheme = https
cluster_formation.consul.svc_addr_auto = true
cluster_formation.consul.svc_addr_use_nodename = false
cluster_formation.consul.svc_addr_nic = eth0
cluster_formation.discovery_retry_limit = 100
cluster_formation.discovery_retry_interval = 1000
log.default.level = debug
