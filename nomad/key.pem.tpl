{{ with $ip_address := (env "attr.unique.network.ip-address") }}
{{ with secret "pki_int/issue/cert" "role_name=rabbitmq" "common_name=rabbitmq.service.consul" "ttl=24h" "alt_names=rabbitmq-management.service.consul,_rabbitmq_management._tcp.service.consul, _rabbitmq._tcp.service.consul,localhost" (printf "ip_sans=127.0.0.1,%s" $ip_address) }}
{{ .Data.private_key }}
{{ end }}{{ end }}