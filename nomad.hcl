job "rabbitmq" {
	datacenters = ["dc1"]
	type        = "service"
	group "rabbitmq" {
		count = 3
		network {
			mode = "host"
			port "rabbitmq" {
				static = 5671
				to     = 5671
			}
			port "rabbitmq-management" {
				static = 15672
				to     = 15672
			}
			port "intercom" {
				static = 25672
				to = 25672
			}
			port "epmd" {
				static = 4369
				to = 4369
			}
		}
		constraint {
			distinct_hosts = true
		}
		service {
			name = "rabbitmq-management"
			tags = ["management"]
			port = "15672"
		}
		task "rabbitmq" {
			vault {
				policies = ["rabbitmq"]
			}
			resources {
				cpu    = 500
				memory = 300
			}
			template {
				data = file("nomad/ca_chain.pem.tpl")
				destination = "local/ca_chain.pem"
			}
			template {
				data = file("nomad/cert.pem.tpl")
				destination = "local/cert.pem"
			}
			template {
				data = file("nomad/key.pem.tpl")
				destination = "secrets/key.pem"
			}
			template {
				data = file("nomad/advanced.config.tpl")
				destination = "local/advanced.config"
			}
			template {
				data = file("nomad/rabbitmq.conf.tpl")
				destination = "local/rabbitmq.conf"
			}
			template {
				data = file("nomad/enabled_plugins.tpl")
				destination = "local/enabled_plugins"
			}
			template {
				data = file("nomad/.erlang.cookie.tpl")
				destination = "secrets/.erlang.cookie"
				perms = "700"
			}
			driver = "docker"
			config {
				image   = "rabbitmq:3.9-management-alpine"
				network_mode="host"
				volumes = [
					"local/enabled_plugins:/etc/rabbitmq/enabled_plugins",
					"local/rabbitmq.conf:/etc/rabbitmq/rabbitmq.conf",
					"local/advanced.config:/etc/rabbitmq/advanced.config",
					"secrets/.erlang.cookie:/var/lib/rabbitmq/.erlang.cookie"
				]
				ports = ["rabbitmq-management", "rabbitmq", "intercom", "epmd"]
			}
			volume_mount {
				volume      = "data"
				destination = "/var/lib/rabbitmq"
			}
		}
		volume "data" {
			type      = "host"
			source    = "rabbitmq"
			read_only = false
		}
	}
}
